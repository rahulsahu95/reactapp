import React, { Component } from 'react';
import ToggleDisplay from 'react-toggle-display';
//import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//import logo from './logo.svg';
import './App.css';
import './form/assets/bootstrap/css/bootstrap.min.css';
import './form/assets/font-awesome/css/font-awesome.min.css';
import './form/assets/css/form-elements.css';
import './form/assets/css/style.css';

class App extends Component {

  constructor() {
    super();
    this.state = { show: false };
  }

  handleClick = () => {
    this.setState({
      show: !this.state.show
    });
  }

  login() {
    console.log("in login");
    window.location="/dashboard.html";
  };

  render() {
    return (
      <div className="top-content">
    <div className="inner-bg">
        <div className="container">
            <div className="row">
                <div className="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>Login &amp; Register Form</strong></h1>
                    <div className="description">
                        <p>
                            This is a free responsive <strong>"login and register forms"</strong> template made with Bootstrap.
                        </p>
                    </div>
                </div>
            </div>
            <div className="row">
              <ToggleDisplay show={!this.state.show}>              
                <div id="loginScreen" className="col-sm-6 col-sm-offset-3">
                    <div className="form-box">
                        <div className="form-top">
                            <div className="form-top-left">
                                <h3>Login to our site</h3>
                                <p>Enter username and password to log on:</p>
                            </div>
                            <div className="form-top-right">
                                <i className="fa fa-lock"></i>
                            </div>
                        </div>
                        <div className="form-bottom">
                            <form role="form" className="login-form" onSubmit={this.login}>
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-username">Username</label>
                                    <input type="text" name="form-username" placeholder="Username" className="form-username form-control" id="form-username" required/>
                                </div>
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-password">Password</label>
                                    <input type="password" name="form-password" placeholder="Password" className="form-password form-control" id="form-password" required/>
                                </div>
                                <button type="submit" className="btn">Sign in!</button>
                            </form>
                        </div>
                    </div>
                    <h4 style={{color:'white',cursor:'pointer'}}>Don't have an account? <a onClick={this.handleClick}>Sign up</a></h4>
                </div>
              </ToggleDisplay>
              
              <ToggleDisplay show={this.state.show}>
                <div id="registrationScreen" className="col-sm-6 col-sm-offset-3">
                    <div className="form-box">
                        <div className="form-top">
                            <div className="form-top-left">
                                <h3>Sign up now</h3>
                                <p>Fill in the form below to get instant access:</p>
                            </div>
                            <div className="form-top-right">
                                <i className="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div className="form-bottom">
                            <form role="form" action="" method="post" className="registration-form">
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-first-name">First name</label>
                                    <input type="text" name="form-first-name" placeholder="First name..." className="form-first-name form-control" id="form-first-name" />
                                </div>
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-last-name">Last name</label>
                                    <input type="text" name="form-last-name" placeholder="Last name..." className="form-last-name form-control" id="form-last-name" />
                                </div>
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-email">Email</label>
                                    <input type="text" name="form-email" placeholder="Email..." className="form-email form-control" id="form-email" />
                                </div>
                                <div className="form-group">
                                    <label className="sr-only" htmlFor="form-about-yourself">About yourself</label>
                                    <textarea name="form-about-yourself" placeholder="About yourself..." className="form-about-yourself form-control" id="form-about-yourself"></textarea>
                                </div>
                                <button type="submit" className="btn">Sign me up!</button>
                            </form>
                        </div>
                        <h4 style={{color:'white',cursor:'pointer'}}>Already have an account? <a onClick={this.handleClick}>Login</a></h4>
                    </div>
                </div>
            </ToggleDisplay>
 
            </div>
        </div>
    </div>
    <footer>
        <div className="container">
            <div className="row">
                <div className="col-sm-8 col-sm-offset-2">
                    <div className="footer-border"></div>
                    <p>copyright &copy; yourdomain.com <i className="fa fa-smile-o"></i></p>
                </div>
            </div>
        </div>
    </footer>
</div>
    );
  }
}

export default App;
